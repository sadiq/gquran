/* settings.c
 *
 * Copyright 2024 Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * Author(s):
 *   Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#undef NDEBUG
#undef G_DISABLE_ASSERT
#undef G_DISABLE_CHECKS
#undef G_DISABLE_CAST_CHECKS
#undef G_LOG_DOMAIN

#include <glib.h>

#include "gq-settings.h"
#include "gq-log.h"

static void
test_settings_geometry (void)
{
  GqSettings *settings;
  GdkRectangle geometry = {100, 200, 300, 400};
  GdkRectangle reset = {0, 0, 0, 0};
  GdkRectangle out;
  gboolean is_maximized;

  settings = gq_settings_new ();
  g_assert_true (GQ_IS_SETTINGS (settings));

  gq_settings_set_window_maximized (settings, 0);
  g_assert_false (gq_settings_get_window_maximized (settings));

  gq_settings_set_window_maximized (settings, 1);
  g_assert_true (gq_settings_get_window_maximized (settings));

  /*
   * gbooleans are typedef to gint.  So test if non boolean
   * values are clamped.
   */
  gq_settings_set_window_maximized (settings, 100);
  is_maximized = gq_settings_get_window_maximized (settings);
  g_assert_cmpint (is_maximized, ==, 1);

  gq_settings_set_window_geometry (settings, &geometry);
  gq_settings_get_window_geometry (settings, &out);
  g_assert_cmpint (out.width, ==, geometry.width);
  g_assert_cmpint (out.height, ==, geometry.height);
  out = reset;
  g_assert_finalize_object (settings);

  /* create a new object, and check again */
  settings = gq_settings_new ();
  g_assert_true (GQ_IS_SETTINGS (settings));

  is_maximized = gq_settings_get_window_maximized (settings);
  g_assert_cmpint (is_maximized, ==, 1);

  gq_settings_get_window_geometry (settings, &out);
  g_assert_cmpint (out.width, ==, geometry.width);
  g_assert_cmpint (out.height, ==, geometry.height);
  g_assert_finalize_object (settings);
}

static void
test_settings_first_run (void)
{
  GqSettings *settings;
  GSettings *gsettings;

  /* Reset the first-run settings */
  gsettings = g_settings_new ("org.sadiqpk.GQuran");
  g_settings_reset (gsettings, "version");
  g_object_unref (gsettings);

  settings = gq_settings_new ();
  g_assert_true (GQ_IS_SETTINGS (settings));
  g_assert_true (gq_settings_get_is_first_run (settings));
  g_object_unref (settings);

  /* create a new object, and check again */
  settings = gq_settings_new ();
  g_assert_true (GQ_IS_SETTINGS (settings));
  g_assert_false (gq_settings_get_is_first_run (settings));
  g_object_unref (settings);

  /*
   * Set a custom version and check, this test assumes that
   * version (ie, PACKAGE_VERSION) is never set to 0.0.0.0
  */
  gsettings = g_settings_new ("org.sadiqpk.GQuran");
  g_settings_set_string (gsettings, "version", "0.0.0.0");
  g_object_unref (gsettings);

  settings = gq_settings_new ();
  g_assert_true (GQ_IS_SETTINGS (settings));
  g_assert_true (gq_settings_get_is_first_run (settings));
  g_object_unref (settings);
}

int
main (int   argc,
      char *argv[])
{
  g_test_init (&argc, &argv, NULL);

  gq_log_init ();
  /* Set enough verbosity */
  gq_log_increase_verbosity ();
  gq_log_increase_verbosity ();
  gq_log_increase_verbosity ();
  gq_log_increase_verbosity ();
  gq_log_increase_verbosity ();

  g_setenv ("GSETTINGS_BACKEND", "memory", TRUE);

  g_test_add_func ("/settings/first_run", test_settings_first_run);
  g_test_add_func ("/settings/geometry", test_settings_geometry);

  return g_test_run ();
}
