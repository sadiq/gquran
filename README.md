<div align="center">
  <a href="https://www.sadiqpk.org/projects/gquran.html">
    <img src="https://gitlab.com/sadiq/gquran/raw/master/data/icons/hicolor/256x256/apps/org.sadiqpk.GQuran.png" width="150" />
  </a>
  <br>

  <a href="https://www.sadiqpk.org/projects/gquran.html"><b>Al Quran</b></a>
  <br>

  A Simple Application to Read Quran
  <br>

  <a href="https://gitlab.com/sadiq/gquran/pipelines"><img
       src="https://gitlab.com/sadiq/gquran/badges/master/pipeline.svg" /></a>
  <a href="https://sadiq.gitlab.io/gquran/coverage"><img
       src="https://gitlab.com/sadiq/gquran/badges/master/coverage.svg" /></a>
</div>

---

Al Quran is an application that helps you read Arabic quran.

Please note that this code is experimental and code shall be force
pushed to this repo.

If you wish to support me, you may kindly donate me at
https://liberapay.com/sadiq/donate

Source Repository: [GitLab][gitlab]

Issues and Feature Requests: [GitLab][issues]

## License

Al Quran is licensed under the GNU GPLv3+. See COPYING for license.
This application is built on top of [My GTemplate][my-gtemplate] which
is available as public domain.

## Building Al Quran

Install required dependencies.

On Debian and derivatives (like Ubuntu):
* `sudo apt install -y build-essential meson libgtk-4-dev \
  appstream-util xsltproc docbook-xsl bash-completion \
  gettext libadwaita-1-dev`

On Fedora and derivatives:
* `sudo dnf install -y @c-development @development-tools \
  gettext-devel gtk4-devel meson desktop-file-utils \
  docbook-style-xsl uncrustify bash-completion libxslt \
  itstool libadwaita-devel`

`meson` is used as the build system.  Run the following to build:
* `meson build`
* `cd build && ninja`
* `sudo ninja install # To install`

Here `build` is a non-existing directory name.  You may use any
name of your choice.

## Running Al Quran

If you have installed application:
* `gquran`

If you want to run the application without installing:
* do `./run` from the build directory

You can add `-v` multiple times to get verbose logs:
* `./run -vvvv`

<!-- Links referenced elsewhere -->
[home]: https://www.sadiqpk.org/projects/gquran.html
[coverage]: https://sadiq.gitlab.io/gquran/coverage
[gitlab]: https://gitlab.com/sadiq/gquran
[github]: https://github.com/pksadiq/gquran
[issues]: https://gitlab.com/sadiq/gquran/issues
[my-gtemplate]: https://www.sadiqpk.org/projects/my-gtemplate.html
