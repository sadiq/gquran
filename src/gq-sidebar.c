/* gq-sidebar.c
 *
 * Copyright 2024 Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * Author(s):
 *   Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "gq-sidebar"

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#define _GNU_SOURCE
#include <string.h>
#include <adwaita.h>

#include "quran/gq-quran.h"
#include "quran/gq-sura.h"
#include "gq-sidebar.h"

struct _GqSidebar
{
  GtkBox    parent_instance;

  GtkWidget *sura_list;

  GqQuran   *quran;
  GqSura    *selected_sura;
  GtkFilter *filter;
  GtkFilterListModel *filter_model;
  GtkSingleSelection *selection_model;
  char *search_needle;
};


G_DEFINE_TYPE (GqSidebar, gq_sidebar, GTK_TYPE_BOX)

enum {
  ITEM_SELECTED,
  N_SIGNALS
};

static guint signals[N_SIGNALS];


static void
setup_list_item_cb (GtkListItemFactory *factory,
                    GtkListItem        *list_item)
{
  GtkWidget *row, *widget;

  row = gtk_grid_new ();
  g_object_set (row,
                "margin-start", 6,
                "margin-end", 6,
                "margin-top", 6,
                "margin-bottom", 6,
                NULL);
  widget = gtk_label_new ("");
  gtk_widget_set_hexpand (widget, TRUE);
  gtk_widget_add_css_class (widget, "quran-font");
  gtk_label_set_xalign (GTK_LABEL (widget), 0.0);
  gtk_label_set_yalign (GTK_LABEL (widget), 0.0);
  gtk_widget_set_direction (widget, GTK_TEXT_DIR_RTL);
  g_object_set_data (G_OBJECT (row), "title", widget);
  gtk_grid_attach (GTK_GRID (row), widget, 0, 0, 2, 1);

  widget = gtk_label_new ("");
  gtk_widget_set_hexpand (widget, TRUE);
  gtk_widget_add_css_class (widget, "dim-label");
  gtk_label_set_xalign (GTK_LABEL (widget), 0.0);
  gtk_label_set_yalign (GTK_LABEL (widget), 0.0);
  g_object_set_data (G_OBJECT (row), "subtitle", widget);
  gtk_grid_attach (GTK_GRID (row), widget, 1, 1, 1, 1);

  gtk_list_item_set_child (list_item, row);
}

static void
bind_list_item_cb (GtkListItemFactory *factory,
                   GtkListItem        *list_item)
{
  GtkWidget *row, *label;
  GqSura *sura;
  const char *name, *name_en;

  row = gtk_list_item_get_child (list_item);
  sura = GQ_SURA (gtk_list_item_get_item (list_item));

  name = gq_sura_get_name (sura);
  name_en = gq_sura_get_name_en (sura);

  label = g_object_get_data (G_OBJECT (row), "title");
  gtk_label_set_label (GTK_LABEL (label), name);

  label = g_object_get_data (G_OBJECT (row), "subtitle");
  gtk_label_set_label (GTK_LABEL (label), name_en);
}

static void
sidebar_sura_selection_changed_cb (GqSidebar *self)
{
  GtkSingleSelection *model;
  guint selected_index;

  g_assert (GQ_IS_SIDEBAR (self));

  model = GTK_SINGLE_SELECTION (gtk_list_view_get_model (GTK_LIST_VIEW (self->sura_list)));
  selected_index = gtk_single_selection_get_selected (model);
  g_clear_object (&self->selected_sura);
  self->selected_sura = g_list_model_get_item (G_LIST_MODEL (model), selected_index);

  g_signal_emit (self, signals[ITEM_SELECTED], 0);
}

static gboolean
sidebar_filter_sura_cb (gpointer item,
                        gpointer user_data)
{
  const char *text;

  GqSidebar *self = user_data;
  GqSura *sura = GQ_SURA (item);

  if (!self->search_needle || !*self->search_needle)
    return TRUE;

  text = gq_sura_get_name_en (sura);
  if (text && strcasestr (text, self->search_needle))
    return TRUE;

  text = gq_sura_get_name (sura);
  if (text && strstr (text, self->search_needle))
    return TRUE;

  return FALSE;
}

static void
sidebar_search_changed_cb (GqSidebar      *self,
                           GtkSearchEntry *entry)
{
  const char *needle;

  g_assert (GQ_IS_SIDEBAR (self));

  needle = gtk_editable_get_text (GTK_EDITABLE (entry));
  g_free (self->search_needle);
  self->search_needle = g_strdup (needle);

  gtk_filter_changed (self->filter, GTK_FILTER_CHANGE_DIFFERENT);
}

static void
gq_sidebar_finalize (GObject *object)
{
  GqSidebar *self = (GqSidebar *)object;

  g_clear_object (&self->quran);
  g_clear_object (&self->selected_sura);

  G_OBJECT_CLASS (gq_sidebar_parent_class)->finalize (object);
}

static void
gq_sidebar_class_init (GqSidebarClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->finalize = gq_sidebar_finalize;

  signals [ITEM_SELECTED] =
    g_signal_new ("item-selected",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0, NULL, NULL, NULL,
                  G_TYPE_NONE, 0);

  gtk_widget_class_set_template_from_resource (widget_class,
                                               "/org/sadiqpk/GQuran/"
                                               "ui/gq-sidebar.ui");

  gtk_widget_class_bind_template_child (widget_class, GqSidebar, sura_list);

  gtk_widget_class_bind_template_callback (widget_class, sidebar_search_changed_cb);
}

static void
gq_sidebar_init (GqSidebar *self)
{
  g_autoptr(GtkListItemFactory) factory = NULL;

  gtk_widget_init_template (GTK_WIDGET (self));

  factory = gtk_signal_list_item_factory_new ();
  g_signal_connect (factory, "setup", G_CALLBACK (setup_list_item_cb), NULL);
  g_signal_connect (factory, "bind", G_CALLBACK (bind_list_item_cb), NULL);
  g_signal_connect (factory, "bind", G_CALLBACK (bind_list_item_cb), NULL);

  gtk_list_view_set_factory (GTK_LIST_VIEW (self->sura_list), factory);

  self->filter = (gpointer)gtk_custom_filter_new (sidebar_filter_sura_cb, self, NULL);
  self->filter_model = gtk_filter_list_model_new (NULL, self->filter);
  self->selection_model = gtk_single_selection_new (G_LIST_MODEL (self->filter_model));

  gtk_list_view_set_model (GTK_LIST_VIEW (self->sura_list),
                           GTK_SELECTION_MODEL (self->selection_model));
  gtk_single_selection_set_autoselect (self->selection_model, FALSE);
  gtk_single_selection_set_model (self->selection_model, G_LIST_MODEL (self->filter_model));
  g_signal_connect_object (self->selection_model, "selection-changed",
                           G_CALLBACK (sidebar_sura_selection_changed_cb), self,
                           G_CONNECT_SWAPPED);
}

void
gq_sidebar_set_quran (GqSidebar *self,
                      GqQuran   *quran)
{
  g_return_if_fail (GQ_IS_SIDEBAR (self));
  g_return_if_fail (GQ_IS_QURAN (quran));

  g_set_object (&self->quran, quran);
  gtk_filter_list_model_set_model (self->filter_model,
                                   gq_quran_get_sura_list (quran));
}

GqSura *
gq_sidebar_get_selected_item (GqSidebar *self)
{
  g_return_val_if_fail (GQ_IS_SIDEBAR (self), NULL);

  return self->selected_sura;
}
