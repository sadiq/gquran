/* gq-utils.c
 *
 * Copyright 2024 Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author(s):
 *   Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "gq-utils"

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "gq-utils.h"

/**
 * SECTION: gq-utils
 * @title: gq-utils
 * @short_description: Generic functions
 * @include: "gq-utils.h"
 *
 * Generic functions that doesn’t fit elsewhere.
 */

/**
 * gq_utils_get_main_thread:
 *
 * Returns the thread-id of the main thread.  Useful
 * to check if current thread is the main UI thread
 * or not.  This is used by GQ_IS_MAIN_THREAD() macro.
 *
 * The first call to this function should be done in the
 * UI thread.  The <function>main()</function> function
 * is a good place to do so.
 *
 * Returns: (transfer none): a #GThread
 */
GThread *
gq_utils_get_main_thread (void)
{
  static GThread *main_thread;

  if (G_UNLIKELY (main_thread == NULL))
    main_thread = g_thread_self ();

  return main_thread;
}

char *
gq_utils_get_arabic_number (guint num)
{
  /* The biggest number we shall have shall be 3 digit */
  char ar_num[65] = {0};
  guint n_bytes = 0;

  while (num)
    {
      guint digit;
      gunichar c;

      digit = num % 10;

      /* The character in the string is Arabic 0 (sifr) */
      c = digit + g_utf8_get_char ("٠");
      n_bytes = n_bytes + g_unichar_to_utf8 (c, ar_num + n_bytes);

      num = num / 10;
    }

  return g_utf8_strreverse (ar_num, -1);
}
