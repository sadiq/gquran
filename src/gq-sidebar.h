/* gq-window.h
 *
 * Copyright 2024 Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * Author(s):
 *   Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>

#include "quran/gq-quran.h"
#include "quran/gq-sura.h"

G_BEGIN_DECLS

#define GQ_TYPE_SIDEBAR (gq_sidebar_get_type ())
G_DECLARE_FINAL_TYPE (GqSidebar, gq_sidebar, GQ, SIDEBAR, GtkBox)

void    gq_sidebar_set_quran         (GqSidebar *self,
                                      GqQuran   *quran);
GqSura *gq_sidebar_get_selected_item (GqSidebar *self);

G_END_DECLS
