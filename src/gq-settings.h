/* gq-settings.h
 *
 * Copyright 2024 Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * Author(s):
 *   Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define GQ_TYPE_SETTINGS (gq_settings_get_type ())

G_DECLARE_FINAL_TYPE (GqSettings, gq_settings, GQ, SETTINGS, GObject)

GqSettings  *gq_settings_new                  (void);
void         gq_settings_save                 (GqSettings   *self);

gboolean     gq_settings_get_is_first_run     (GqSettings   *self);
gboolean     gq_settings_get_window_maximized (GqSettings   *self);
void         gq_settings_set_window_maximized (GqSettings   *self,
                                               gboolean      maximized);
void         gq_settings_get_window_geometry  (GqSettings   *self,
                                               GdkRectangle *geometry);
void         gq_settings_set_window_geometry  (GqSettings   *self,
                                               GdkRectangle *geometry);
void         gq_settings_set_font             (GqSettings   *self,
                                               const char   *font_name);
const char  *gq_settings_get_font             (GqSettings   *self);

G_END_DECLS
