/* gq-settings.c
 *
 * Copyright 2024 Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * Author(s):
 *   Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "gq-settings"

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "gq-settings.h"
#include "gq-log.h"

/**
 * SECTION: gq-settings
 * @title: GqSettings
 * @short_description: The Application settings
 * @include: "gq-settings.h"
 *
 * A class that handles application specific settings, and
 * to store them to disk.
 */

struct _GqSettings
{
  GObject    parent_instance;

  GSettings *app_settings;
  gboolean   first_run;
};

G_DEFINE_TYPE (GqSettings, gq_settings, G_TYPE_OBJECT)


static void
gq_settings_dispose (GObject *object)
{
  GqSettings *self = (GqSettings *)object;

  GQ_TRACE_MSG ("disposing settings");

  g_settings_set_string (self->app_settings, "version", PACKAGE_VERSION);
  g_settings_apply (self->app_settings);

  G_OBJECT_CLASS (gq_settings_parent_class)->dispose (object);
}

static void
gq_settings_class_init (GqSettingsClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = gq_settings_dispose;
}

static void
gq_settings_init (GqSettings *self)
{
  g_autofree char *version = NULL;

  self->app_settings = g_settings_new (PACKAGE_ID);
  version = g_settings_get_string (self->app_settings, "version");

  if (!g_str_equal (version, PACKAGE_VERSION))
    self->first_run = TRUE;

  g_settings_delay (self->app_settings);
}

/**
 * gq_settings_new:
 *
 * Create a new #GqSettings
 *
 * Returns: (transfer full): A #GqSettings.
 * Free with g_object_unref().
 */
GqSettings *
gq_settings_new (void)
{
  return g_object_new (GQ_TYPE_SETTINGS, NULL);
}

/**
 * gq_settings_save:
 * @self: A #GqSettings
 *
 * Save modified settings to disk.  By default,
 * the modified settings are saved to disk only
 * when #GqSettings is disposed.  Use this
 * to force save to disk.
 */
void
gq_settings_save (GqSettings *self)
{
  g_return_if_fail (GQ_IS_SETTINGS (self));

  g_settings_apply (self->app_settings);
}

/**
 * gq_settings_get_is_first_run:
 * @self: A #GqSettings
 *
 * Get if the application has ever launched after install
 * or update.
 *
 * Returns: %TRUE for the first launch of application after
 * install or update.  %FALSE otherwise.
 */
gboolean
gq_settings_get_is_first_run (GqSettings *self)
{
  g_return_val_if_fail (GQ_IS_SETTINGS (self), FALSE);

  return self->first_run;
}

/**
 * gq_settings_get_window_maximized:
 * @self: A #GqSettings
 *
 * Get the window maximized state as saved in @self.
 *
 * Returns: %TRUE if maximized.  %FALSE otherwise.
 */
gboolean
gq_settings_get_window_maximized (GqSettings *self)
{
  g_return_val_if_fail (GQ_IS_SETTINGS (self), FALSE);

  return g_settings_get_boolean (self->app_settings, "window-maximized");
}

/**
 * gq_settings_set_window_maximized:
 * @self: A #GqSettings
 * @maximized: The window state to save
 *
 * Set the window maximized state in @self.
 */
void
gq_settings_set_window_maximized (GqSettings *self,
                                   gboolean     maximized)
{
  g_return_if_fail (GQ_IS_SETTINGS (self));

  g_settings_set_boolean (self->app_settings, "window-maximized", !!maximized);
}

/**
 * gq_settings_get_window_geometry:
 * @self: A #GqSettings
 * @geometry: (out): A #GdkRectangle
 *
 * Get the window geometry as saved in @self.
 */
void
gq_settings_get_window_geometry (GqSettings  *self,
                                  GdkRectangle *geometry)
{
  g_return_if_fail (GQ_IS_SETTINGS (self));
  g_return_if_fail (geometry != NULL);

  g_settings_get (self->app_settings, "window-size", "(ii)",
                  &geometry->width, &geometry->height);
  geometry->x = geometry->y = -1;
}

/**
 * gq_settings_set_window_geometry:
 * @self: A #GqSettings
 * @geometry: A #GdkRectangle
 *
 * Set the window geometry in @self.
 */
void
gq_settings_set_window_geometry (GqSettings  *self,
                                  GdkRectangle *geometry)
{
  g_return_if_fail (GQ_IS_SETTINGS (self));
  g_return_if_fail (geometry != NULL);

  g_settings_set (self->app_settings, "window-size", "(ii)",
                  geometry->width, geometry->height);
}

void
gq_settings_set_font (GqSettings *self,
                      const char *font_name)
{
  g_return_if_fail (GQ_IS_SETTINGS (self));

  g_settings_set_string (self->app_settings, "font", font_name);
}

const char *
gq_settings_get_font (GqSettings *self)
{
  g_return_val_if_fail (GQ_IS_SETTINGS (self), NULL);

  /* if (!self->) */

  return NULL;
}
