/* gq-main-view.c
 *
 * Copyright 2024 Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * Author(s):
 *   Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "gq-main-view"

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <adwaita.h>

#include "quran/gq-aya.h"
#include "gq-main-view.h"

struct _GqMainView
{
  GtkBox parent_instance;

  GtkWidget *main_stack;
  GtkWidget *main_status;
  GtkWidget *sura_view;
  GtkWidget *view_clamp;
  GtkWidget *sura_textview;

  GtkTextBuffer *buffer;
  GtkTextTag *zoom_tag;
  GtkTextTag *font_tag;

  GqSura *sura;
  gulong fonts_id;
  bool has_font;
};


G_DEFINE_TYPE (GqMainView, gq_main_view, GTK_TYPE_BOX)

static void
view_scale_changed_cb (GqMainView *self,
                       double      scale)
{
  double scale_now;

  g_assert (GQ_IS_MAIN_VIEW (self));

  g_object_get (self->zoom_tag, "scale", &scale_now, NULL);
  scale_now = scale_now + (scale - 1.0) / 10.0;
  scale_now = CLAMP (scale_now, 1.0, 8.0);
  g_object_set (self->zoom_tag, "scale", scale_now, NULL);
}

static void
scaled_changed_cb (GqMainView *self)
{
  double scale_now;
  guint size;

  g_assert (GQ_IS_MAIN_VIEW (self));

  g_object_get (self->zoom_tag, "scale", &scale_now, NULL);
  size = scale_now / 2.5 * 600;
  adw_clamp_set_maximum_size (ADW_CLAMP (self->view_clamp), size);
}

static void
gq_main_view_finalize (GObject *object)
{
  GqMainView *self = (GqMainView *)object;

  g_clear_object (&self->sura);

  G_OBJECT_CLASS (gq_main_view_parent_class)->finalize (object);
}

static void
gq_main_view_class_init (GqMainViewClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->finalize = gq_main_view_finalize;

  gtk_widget_class_set_template_from_resource (widget_class,
                                               "/org/sadiqpk/GQuran/"
                                               "ui/gq-main-view.ui");

  gtk_widget_class_bind_template_child (widget_class, GqMainView, main_stack);
  gtk_widget_class_bind_template_child (widget_class, GqMainView, main_status);
  gtk_widget_class_bind_template_child (widget_class, GqMainView, sura_view);
  gtk_widget_class_bind_template_child (widget_class, GqMainView, view_clamp);
  gtk_widget_class_bind_template_child (widget_class, GqMainView, sura_textview);

  gtk_widget_class_bind_template_child (widget_class, GqMainView, buffer);
  gtk_widget_class_bind_template_child (widget_class, GqMainView, zoom_tag);
  gtk_widget_class_bind_template_child (widget_class, GqMainView, font_tag);

  gtk_widget_class_bind_template_callback (widget_class, scaled_changed_cb);
}

static void
gq_main_view_init (GqMainView *self)
{
  GtkGesture *gesture;

  gtk_widget_init_template (GTK_WIDGET (self));

  gesture = gtk_gesture_zoom_new ();
  g_signal_connect_object (gesture, "scale-changed",
                           G_CALLBACK (view_scale_changed_cb),
                           self, G_CONNECT_SWAPPED);
  gtk_event_controller_set_propagation_phase (GTK_EVENT_CONTROLLER (gesture),
                                              GTK_PHASE_CAPTURE);
  gtk_widget_add_controller (self->sura_view, GTK_EVENT_CONTROLLER (gesture));
}

void
gq_main_view_set_sura (GqMainView *self,
                       GqSura     *sura)
{
  GPtrArray *ayas;
  guint8 sura_index;

  g_return_if_fail (GQ_IS_MAIN_VIEW (self));
  g_return_if_fail (!sura || GQ_IS_SURA (sura));

  if (!g_set_object (&self->sura, sura))
    return;

  if (!sura)
    {
      adw_view_stack_set_visible_child (ADW_VIEW_STACK (self->main_stack), self->main_status);
      return;
    }

  ayas = gq_sura_get_ayas (sura);
  g_assert (ayas);

  gtk_text_buffer_set_text (self->buffer, "", 0);

  sura_index = gq_sura_get_index (sura);

  /* Insert bismi unless it's the first or 9th sura */
  if (sura_index != 1 && sura_index != 9)
    {
      const char *bismi = "بِسْمِ ٱللَّهِ ٱلرَّحْمَـٰنِ ٱلرَّحِيمِ";
      GtkTextIter start, end;

      gtk_text_buffer_insert_at_cursor (self->buffer, bismi, -1);
      gtk_text_buffer_get_bounds (self->buffer, &start, &end);
      gtk_text_buffer_apply_tag_by_name (self->buffer, "bismi", &start, &end);
      gtk_text_buffer_insert_at_cursor (self->buffer, "\n", -1);
    }

  for (guint i = 0; i < ayas->len; i++)
    {
      g_autofree char *num = NULL;
      const char *text;
      GqAya *aya;

      aya = ayas->pdata[i];
      text = gq_aya_get_text (aya);
      gtk_text_buffer_insert_at_cursor (self->buffer, text, -1);
      num = g_strdup_printf (" %s ", gq_aya_get_index_ar (aya));
      gtk_text_buffer_insert_at_cursor (self->buffer, num, -1);
    }

  {
    GtkTextIter start, end;

    gtk_text_buffer_get_bounds (self->buffer, &start, &end);
    gtk_text_buffer_apply_tag_by_name (self->buffer, "line", &start, &end);
    gtk_text_buffer_apply_tag (self->buffer, self->font_tag, &start, &end);
    gtk_text_buffer_apply_tag (self->buffer, self->zoom_tag, &start, &end);
  }

  adw_view_stack_set_visible_child (ADW_VIEW_STACK (self->main_stack), self->sura_view);
}

GqSura *
gq_main_view_get_sura (GqMainView *self)
{
  g_return_val_if_fail (GQ_IS_MAIN_VIEW (self), NULL);

  return self->sura;
}
