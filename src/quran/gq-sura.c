/* gq-sura.c
 *
 * Copyright 2024 Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * Author(s):
 *   Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "gq-sura"

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "gq-sura.h"

struct _GqSura
{
  GObject    parent_instance;

  char *name_ar;
  char *name_en;
  char *transliteration_en;

  GPtrArray *ayas;

  guint8 sura_index;
};


G_DEFINE_TYPE (GqSura, gq_sura, G_TYPE_OBJECT)

static void
gq_sura_finalize (GObject *object)
{
  GqSura *self = (GqSura *)object;

  g_clear_pointer (&self->name_ar, g_free);
  g_clear_pointer (&self->name_en, g_free);
  g_clear_pointer (&self->transliteration_en, g_free);

  G_OBJECT_CLASS (gq_sura_parent_class)->finalize (object);
}

static void
gq_sura_class_init (GqSuraClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = gq_sura_finalize;
}

static void
gq_sura_init (GqSura *self)
{
}

GqSura *
gq_sura_new (const char *name_ar,
             const char *name_en,
             guint8      sura_index)
{
  GqSura *self;

  g_return_val_if_fail (name_ar && *name_ar, NULL);
  g_return_val_if_fail (name_en && *name_en, NULL);
  g_return_val_if_fail (sura_index >= 1 && sura_index <= 114, NULL);

  self = g_object_new (GQ_TYPE_SURA, NULL);
  self->name_ar = g_strdup (name_ar);
  self->name_en = g_strdup (name_en);
  self->sura_index = sura_index;

  return self;
}

const char *
gq_sura_get_name (GqSura *self)
{
  g_return_val_if_fail (GQ_IS_SURA (self), NULL);

  return self->name_ar;
}

const char *
gq_sura_get_name_en (GqSura *self)
{
  g_return_val_if_fail (GQ_IS_SURA (self), NULL);

  return self->name_en;
}

guint8
gq_sura_get_index (GqSura *self)
{
  g_return_val_if_fail (GQ_IS_SURA (self), 0);

  return self->sura_index;
}

GPtrArray *
gq_sura_get_ayas (GqSura *self)
{
  g_return_val_if_fail (GQ_IS_SURA (self), NULL);

  return self->ayas;
}

void
gq_sura_set_ayas (GqSura    *self,
                  GPtrArray *ayas)
{
  g_return_if_fail (GQ_IS_SURA (self));

  if (self->ayas)
    return;

  self->ayas = ayas;
}
