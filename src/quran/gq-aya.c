/* gq-aya.c
 *
 * Copyright 2024 Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * Author(s):
 *   Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "gq-aya"

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "gq-sura.h"
#include "gq-aya.h"

struct _GqAya
{
  GObject     parent_instance;

  GqSura     *sura;
  char       *text_ar;

  char       *translation;
  GRefString *translation_lang;
  GRefString *translation_author;

  char       *aya_index_ar;
  guint16     aya_index;

};


G_DEFINE_TYPE (GqAya, gq_aya, G_TYPE_OBJECT)

static void
gq_aya_finalize (GObject *object)
{
  GqAya *self = (GqAya *)object;

  g_clear_pointer (&self->text_ar, g_free);
  g_clear_pointer (&self->aya_index_ar, g_free);

  G_OBJECT_CLASS (gq_aya_parent_class)->finalize (object);
}

static void
gq_aya_class_init (GqAyaClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = gq_aya_finalize;
}

static void
gq_aya_init (GqAya *self)
{
}

/**
 * gq_aya_new:
 * @sura: A #GqSura
 * @text: The Arabic text of the aya
 * @aya_index: The index of aya in sura
 *
 * Create a new #GqAya. The @aya_index starts from 1.
 */
GqAya *
gq_aya_new (gpointer    sura,
            const char *text,
            guint16     aya_index,
            const char *aya_index_ar)
{
  GqAya *self;

  g_return_val_if_fail (GQ_IS_SURA (sura), NULL);
  g_return_val_if_fail (text && *text, NULL);
  g_return_val_if_fail (aya_index > 0 && aya_index < 300, NULL);
  g_return_val_if_fail (aya_index_ar && *aya_index_ar, NULL);

  self = g_object_new (GQ_TYPE_AYA, NULL);

  g_set_weak_pointer (&self->sura, sura);
  self->text_ar = g_strdup (text);
  self->aya_index = aya_index;
  self->aya_index_ar = g_strdup (aya_index_ar);

  return self;
}

const char *
gq_aya_get_text (GqAya *self)
{
  g_return_val_if_fail (GQ_IS_AYA (self), NULL);

  return self->text_ar;
}

const char *
gq_aya_get_index_ar (GqAya *self)
{
  g_return_val_if_fail (GQ_IS_AYA (self), NULL);

  return self->aya_index_ar;
}
