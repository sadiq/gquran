/* gq-quran.h
 *
 * Copyright 2024 Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * Author(s):
 *   Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gio/gio.h>

G_BEGIN_DECLS

#define GQ_TYPE_QURAN (gq_quran_get_type ())
G_DECLARE_FINAL_TYPE (GqQuran, gq_quran, GQ, QURAN, GObject)

GqQuran    *gq_quran_new              (void);
void        gq_quran_set_sura_list   (GqQuran   *self,
                                      GPtrArray *sura_array);
GListModel *gq_quran_get_sura_list   (GqQuran   *self);

G_END_DECLS
