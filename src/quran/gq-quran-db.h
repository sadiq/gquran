/* gq-quran-db.h
 *
 * Copyright 2024 Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * Author(s):
 *   Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gio/gio.h>

#include "gq-sura.h"

G_BEGIN_DECLS

/* increment when DB changes */
#define DB_VERSION 1

#define GQ_TYPE_QURAN_DB (gq_quran_db_get_type ())
G_DECLARE_FINAL_TYPE (GqQuranDb, gq_quran_db, GQ, QURAN_DB, GObject)

GqQuranDb   *gq_quran_db_new                 (char                 *dir,
                                              const char           *file_name);
void         gq_quran_db_open_async          (GqQuranDb            *self,
                                              GCancellable         *cancellable,
                                              GAsyncReadyCallback   callback,
                                              gpointer              user_data);
gboolean    gq_quran_db_open_finish          (GqQuranDb            *self,
                                              GAsyncResult         *result,
                                              GError              **error);
void        gq_quran_db_load_async           (GqQuranDb            *self,
                                              GCancellable         *cancellable,
                                              GAsyncReadyCallback   callback,
                                              gpointer              user_data);
gpointer    gq_quran_db_load_finish          (GqQuranDb            *self,
                                              GAsyncResult         *result,
                                              GError              **error);
void        gq_quran_db_load_sura_async      (GqQuranDb            *self,
                                              gpointer              sura,
                                              GCancellable         *cancellable,
                                              GAsyncReadyCallback   callback,
                                              gpointer              user_data);
gpointer    gq_quran_db_load_sura_finish     (GqQuranDb            *self,
                                              GAsyncResult         *result,
                                              GError              **error);

G_END_DECLS
