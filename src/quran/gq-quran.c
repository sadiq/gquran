/* gq-quran.c
 *
 * Copyright 2024 Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * Author(s):
 *   Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "gq-quran"

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "quran/gq-sura.h"
#include "quran/gq-quran.h"

#define TOTAL_SURA_COUNT 114

struct _GqQuran
{
  GObject             parent_instance;
  GListStore         *list_of_sura;
};


G_DEFINE_TYPE (GqQuran, gq_quran, G_TYPE_OBJECT)

static void
gq_quran_finalize (GObject *object)
{
  GqQuran *self = (GqQuran *)object;

  g_list_store_remove_all (self->list_of_sura);
  g_clear_object (&self->list_of_sura);

  G_OBJECT_CLASS (gq_quran_parent_class)->finalize (object);
}

static void
gq_quran_class_init (GqQuranClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = gq_quran_finalize;
}

static void
gq_quran_init (GqQuran *self)
{
  self->list_of_sura = g_list_store_new (GQ_TYPE_SURA);
}

GqQuran *
gq_quran_new (void)
{
  return g_object_new (GQ_TYPE_QURAN, NULL);
}

void
gq_quran_set_sura_list (GqQuran   *self,
                        GPtrArray *sura_array)
{
  g_return_if_fail (GQ_IS_QURAN (self));
  g_return_if_fail (sura_array);
  g_assert (sura_array->len == TOTAL_SURA_COUNT);

  g_list_store_remove_all (self->list_of_sura);
  g_list_store_splice (self->list_of_sura, 0, 0,
                       sura_array->pdata, sura_array->len);
}

GListModel *
gq_quran_get_sura_list (GqQuran *self)
{
  g_return_val_if_fail (GQ_IS_QURAN (self), NULL);

  return G_LIST_MODEL (self->list_of_sura);
}
