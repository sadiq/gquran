/* gq-sura.h
 *
 * Copyright 2024 Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * Author(s):
 *   Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <stdbool.h>
#include <glib-object.h>

G_BEGIN_DECLS

#define GQ_TYPE_SURA (gq_sura_get_type ())
G_DECLARE_FINAL_TYPE (GqSura, gq_sura, GQ, SURA, GObject)

GqSura *gq_sura_new                  (const char *name_ar,
                                      const char *name_en,
                                      guint8      sura_index);
const char *gq_sura_get_name         (GqSura     *self);
const char *gq_sura_get_name_en      (GqSura     *self);
const char *gq_sura_get_translation  (GqSura     *self,
                                      GRefString *lang);
guint8      gq_sura_get_index        (GqSura     *self);
bool        gq_sura_matches_string   (GqSura     *self,
                                      const char *needle);
GPtrArray  *gq_sura_get_ayas         (GqSura     *self);
void        gq_sura_set_ayas         (GqSura     *self,
                                      GPtrArray  *ayas);

G_END_DECLS
