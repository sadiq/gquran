/* gq-aya.h
 *
 * Copyright 2024 Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * Author(s):
 *   Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

#define GQ_TYPE_AYA (gq_aya_get_type ())
G_DECLARE_FINAL_TYPE (GqAya, gq_aya, GQ, AYA, GObject)

GqAya       *gq_aya_new             (gpointer    sura,
                                     const char *text_ar,
                                     guint16     aya_index,
                                     const char *aya_index_ar);
const char  *gq_aya_get_text        (GqAya      *self);
const char  *gq_aya_get_index_ar    (GqAya      *self);
gpointer     gq_aya_get_sura        (GqAya      *self,
                                     guint16    *out_index);
void         gq_aya_set_translation (GqAya      *self,
                                     const char *text,
                                     GRefString *lang,
                                     GRefString *author);
const char  *gq_aya_get_translation (GqAya      *self,
                                     GRefString *lang,
                                     GRefString *author);

G_END_DECLS
