/* gq-quran-db.c
 *
 * Copyright 2024 Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * Author(s):
 *   Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "gq-quran-db"

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <glib.h>
#include <glib/gstdio.h>
#include <sqlite3.h>

#include "gq-utils.h"
#include "gq-xml.h"
#include "gq-aya.h"
#include "gq-quran.h"
#include "gq-quran-db.h"

#define STRING(arg) STRING_VALUE(arg)
#define STRING_VALUE(arg) #arg

struct _GqQuranDb
{
  GObject    parent_instance;

  GAsyncQueue *queue;
  GThread     *worker_thread;
  sqlite3     *db;
  char        *db_path;

  gboolean    db_exists;
};


typedef void (*GqQuranDbCallback) (GqQuranDb *self,
                                   GTask     *task);

G_DEFINE_TYPE (GqQuranDb, gq_quran_db, G_TYPE_OBJECT)

static int
gq_quran_db_get_db_version (GqQuranDb *self,
                            sqlite3   *db)
{
  sqlite3_stmt *stmt;
  int status, version;

  g_assert (GQ_IS_QURAN_DB (self));
  g_assert (g_thread_self () == self->worker_thread);

  sqlite3_prepare_v2 (db, "PRAGMA user_version;", -1, &stmt, NULL);
  status = sqlite3_step (stmt);
  g_assert (status == SQLITE_ROW);

  version = sqlite3_column_int (stmt, 0);

  sqlite3_finalize (stmt);

  return version;
}

static void
gq_quran_db_close_db (GqQuranDb *self,
                      GTask     *task)
{
  sqlite3 *db;
  int status;

  g_assert (GQ_IS_QURAN_DB (self));
  g_assert (G_IS_TASK (task));
  g_assert (g_thread_self () == self->worker_thread);
  g_assert (self->db);

  db = g_steal_pointer (&self->db);
  status = sqlite3_close (db);

  if (status == SQLITE_OK) {
    g_clear_pointer (&self->worker_thread, g_thread_unref);
    g_debug ("Database closed successfully");
    g_task_return_boolean (task, TRUE);
  } else {
    g_task_return_new_error (task,
                             G_IO_ERROR,
                             G_IO_ERROR_FAILED,
                             "Database could not be closed. errno: %d, desc: %s",
                             status, sqlite3_errmsg (db));
  }
}

static void
gq_quran_db_open_db (GqQuranDb *self,
                     GTask     *task)
{
  sqlite3 *db;
  int status;
  gboolean db_exists;

  g_assert (GQ_IS_QURAN_DB (self));
  g_assert (G_IS_TASK (task));
  g_assert (g_thread_self () == self->worker_thread);
  g_assert (!self->db);

  db_exists = g_file_test (self->db_path, G_FILE_TEST_EXISTS);

  if (!db_exists)
    {
      g_task_return_new_error (task,
                               G_IO_ERROR,
                               G_IO_ERROR_NOT_INITIALIZED,
                               "Db not created");
      return;
    }

  status = sqlite3_open (self->db_path, &db);

  if (status == SQLITE_OK) {
    int version;

    version = gq_quran_db_get_db_version (self, db);
    sqlite3_close (db);

    if (version != DB_VERSION)
      g_task_return_new_error (task,
                               G_IO_ERROR,
                               G_IO_ERROR_PENDING,
                               "Db has to be migrated to new version");
    else
      g_task_return_boolean (task, TRUE);
  } else {
    g_task_return_new_error (task,
                             G_IO_ERROR,
                             G_IO_ERROR_FAILED,
                             "Couldn't open database errno: %d, error: %s",
                             status, sqlite3_errmsg (self->db));
  }
}

static bool
gq_quran_db_create_schema (GqQuranDb *self,
                           sqlite3   *db,
                           GTask     *task)
{
  char *error = NULL;
  const char *sql;
  int status;

  g_assert (GQ_IS_QURAN_DB (self));
  g_assert (G_IS_TASK (task));
  g_assert (g_thread_self () == self->worker_thread);

  sql =
    "CREATE TABLE IF NOT EXISTS sura ("
    "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "
    "sura_index INTEGER NOT NULL UNIQUE, "
    "sura_index_ar TEXT NOT NULL UNIQUE, "
    "name TEXT NOT NULL UNIQUE, "
    "name_en TEXT NOT NULL, "
    "transliteration_en TEXT NOT NULL UNIQUE, "
    "aya_count INTEGER NOT NULL"
    /* "relevation_order INTEGER NOT NULL UNIQUE, " */
    /* "rukus_count INTEGER NOT NULL, " */
    /* "manzil INTEGER NOT NULL, " */
    /* "type INTEGER NOT NULL" */
    ");"

    "CREATE TABLE IF NOT EXISTS aya ("
    "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "
    "sura_id INTEGER NOT NULL REFERENCES sura(id) ON DELETE CASCADE, "
    "text TEXT NOT NULL, "
    /* "transliteration_en TEXT NOT NULL, " */
    /* "juz INTEGER NOT NULL, " */
    /* "hizb INTEGER NOT NULL, " */
    /* "has_ruku BOOLEAN NOT NULL CHECK (has_ruku IN (0, 1)) DEFAULT 0, " */
    /* "sajada INTEGER, " */
    "aya_index INTEGER NOT NULL, "
    "aya_index_ar TEXT NOT NULL, "
    /* "page INTEGER NOT NULL" */
    "UNIQUE (sura_id, aya_index));"

    /* "CREATE TABLE IF NOT EXISTS author (" */
    /* "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " */
    /* "name_en TEXT NOT NULL UNIQUE" */
    /* ");" */

    /* "CREATE TABLE IF NOT EXISTS language (" */
    /* "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " */
    /* /\* ISO 639 language code *\/ */
    /* "lang_code TEXT NOT NULL UNIQUE, " */
    /* "name_en TEXT NOT NULL, " */
    /* "name TEXT" */
    /* ");" */

    /* "CREATE TABLE IF NOT EXISTS translation_book (" */
    /* "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " */
    /* "author_id INTEGER NOT NULL REFERENCES author(id) ON DELETE CASCADE, " */
    /* "language_id INTEGER NOT NULL REFERENCES language(id) ON DELETE CASCADE, " */
    /* "UNIQUE (author_id, language_id));" */

    /* "CREATE TABLE IF NOT EXISTS translation (" */
    /* "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " */
    /* "book_id INTEGER NOT NULL REFERENCES translation_book(id) ON DELETE CASCADE, " */
    /* "sura_id INTEGER NOT NULL REFERENCES sura(id) ON DELETE CASCADE, " */
    /* /\* If it's the translation of sura name, aya_id should be NULL *\/ */
    /* "aya_id INTEGER REFERENCES aya(id) ON DELETE CASCADE, " */
    /* "translation TEXT NOT NULL, " */
    /* "UNIQUE (sura_id, book_id, aya_id));" */

    /* "CREATE TABLE IF NOT EXISTS audio_book (" */
    /* "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " */
    /* "author_id INTEGER NOT NULL REFERENCES author(id) ON DELETE CASCADE, " */
    /* "language_id INTEGER NOT NULL REFERENCES author(id) ON DELETE CASCADE, " */
    /* "path TEXT NOT NULL" */
    /* "UNIQUE (author_id, language_id));" */

    "PRAGMA user_version = " STRING (DB_VERSION) ";";

  status = sqlite3_exec (db, sql, NULL, NULL, &error);

  if (status == SQLITE_OK)
    return true;

  g_warning ("error: %s", error);
  g_task_return_new_error (task,
                           G_IO_ERROR,
                           G_IO_ERROR_FAILED,
                           "Couldn't create tables. errno: %d, desc: %s. %s",
                           status, sqlite3_errmsg (db), error);
 sqlite3_free (error);

  return false;
}

static void
process_sura (sqlite3 *db,
              XmlNode *node,
              int      sura_id)
{
  sqlite3_stmt *stmt;
  const char *sql;
  int status;

  sql = "INSERT INTO aya(aya_index,aya_index_ar,sura_id,text) "
        "VALUES(?1,?2,?3,?4);";

  do
    {
      g_autofree char *index_ar = NULL;
      g_autoptr(XmlChar) text = NULL;
      int index;

      text = xml_node_get_prop (node, "text");
      index = xml_node_get_int (node, "index");
      index_ar = gq_utils_get_arabic_number (index);

      sqlite3_prepare_v2 (db, sql, -1, &stmt, NULL);
      sqlite3_bind_int (stmt, 1, index);
      sqlite3_bind_text (stmt, 2, (char *)index_ar, -1, SQLITE_TRANSIENT);
      sqlite3_bind_int (stmt, 3, sura_id);
      sqlite3_bind_text (stmt, 4, (char *)text, -1, SQLITE_TRANSIENT);

      /* can use the same prepare and do multiple steps? */
      status = sqlite3_step (stmt);
      g_assert (status == SQLITE_DONE);
      sqlite3_finalize (stmt);

    } while ((node = node->next));
}

static void
process_sura_list (sqlite3 *db,
                   XmlNode *node)
{
  sqlite3_stmt *stmt;
  const char *sql;
  int status;

  sql = "INSERT INTO sura(sura_index,sura_index_ar,name,name_en,transliteration_en,aya_count) "
        "VALUES(?1,?2,?3,?4,?5,?6);";

  do
    {
      g_autoptr(XmlChar) translation_en = NULL;
      g_autoptr(XmlChar) rukus_count = NULL;
      g_autoptr(XmlChar) name_ar = NULL;
      g_autoptr(XmlChar) name_en = NULL;
      g_autofree char *index_ar = NULL;
      int index, aya_count;

      name_ar = xml_node_get_prop (node, "name");
      name_en = xml_node_get_prop (node, "ename");
      translation_en = xml_node_get_prop (node, "tname");

      index = xml_node_get_int (node, "index");
      index_ar = gq_utils_get_arabic_number (index);
      aya_count = xml_node_get_int (node, "ayas");

      sqlite3_prepare_v2 (db, sql, -1, &stmt, NULL);
      sqlite3_bind_int (stmt, 1, index);
      sqlite3_bind_text (stmt, 2, (char *)index_ar, -1, SQLITE_TRANSIENT);
      sqlite3_bind_text (stmt, 3, (char *)name_ar, -1, SQLITE_TRANSIENT);
      sqlite3_bind_text (stmt, 4, (char *)name_en, -1, SQLITE_TRANSIENT);
      sqlite3_bind_text (stmt, 5, (char *)translation_en, -1, SQLITE_TRANSIENT);
      sqlite3_bind_int (stmt, 6, aya_count);

      /* can use the same prepare and do multiple steps? */
      status = sqlite3_step (stmt);
      g_assert (status == SQLITE_DONE);
      sqlite3_finalize (stmt);

    } while ((node = node->next));
}

static bool
gq_quran_populate_db (GqQuranDb *self,
                      sqlite3   *db,
                      GTask     *task)
{
  g_autoptr(GBytes) quran_xml = NULL;
  g_autoptr(GBytes) sura_xml = NULL;
  g_autoptr(XmlDoc) quran_doc = NULL;
  g_autoptr(XmlDoc) sura_doc = NULL;
  GError *error = NULL;
  gconstpointer bytes;
  XmlNode *node;
  gsize size;

  g_assert (GQ_IS_QURAN_DB (self));
  g_assert (G_IS_TASK (task));
  g_assert (g_thread_self () == self->worker_thread);

  quran_xml = g_resources_lookup_data ("/org/sadiqpk/GQuran/quran-index.xml", 0, &error);

  if (error)
    {
      g_task_return_error (task, error);
      return false;
    }

  bytes = g_bytes_get_data (quran_xml, &size);
  quran_doc = xml_doc_new (bytes, size);
  g_assert (quran_doc);

  node = xml_doc_get_root (quran_doc);
  g_assert (node);

  node = node->children;
  g_assert (node);

  sqlite3_exec (db, "BEGIN TRANSACTION;", NULL, NULL, NULL);

  do
    {
      const char *name;

      name = (char *)node->name;

      if (g_strcmp0 (name, "suras") == 0)
        process_sura_list (db, node->children);
    } while ((node = node->next));

  sqlite3_exec (db, "END TRANSACTION;", NULL, NULL, NULL);

  sura_xml = g_resources_lookup_data ("/org/sadiqpk/GQuran/quran-content.xml", 0, &error);

  if (error)
    {
      g_task_return_error (task, error);
      return false;
    }

  bytes = g_bytes_get_data (sura_xml, &size);
  sura_doc = xml_doc_new (bytes, size);
  g_assert (sura_doc);

  node = xml_doc_get_root (sura_doc);
  g_assert (node);

  node = node->children;
  g_assert (node);

  sqlite3_exec (db, "BEGIN TRANSACTION;", NULL, NULL, NULL);

  do
    {
      const char *name;

      name = (char *)node->name;

      if (g_strcmp0 (name, "sura") == 0)
        process_sura (db, node->children, xml_node_get_int (node, "index"));
    } while ((node = node->next));

  sqlite3_exec (db, "END TRANSACTION;", NULL, NULL, NULL);

  return true;
}

static GqQuran *
gq_quran_db_load_quran (GqQuranDb *self)
{
  g_autoptr(GPtrArray) sura_list = NULL;
  sqlite3_stmt *stmt;
  GqQuran *quran;
  GqSura *sura;

  g_assert (GQ_IS_QURAN_DB (self));
  g_assert (self->db);

  sura_list = g_ptr_array_new_with_free_func (g_object_unref);

  sqlite3_prepare_v2 (self->db,
                      "SELECT id,name,transliteration_en FROM sura ORDER BY id;",
                      -1, &stmt, NULL);
  while (sqlite3_step (stmt) == SQLITE_ROW)
    {
      const char *name, *name_en;
      int id;

      id = sqlite3_column_int (stmt, 0);
      name = (gpointer)sqlite3_column_text (stmt, 1);
      name_en = (gpointer)sqlite3_column_text (stmt, 2);

      sura = gq_sura_new (name, name_en, id);
      g_ptr_array_add (sura_list, sura);

      g_assert (sura_list->len == (guint)id);
    }

  quran = gq_quran_new ();
  gq_quran_set_sura_list (quran, sura_list);

  return quran;
}

static void
gq_quran_db_load_sura (GqQuranDb *self,
                       GTask     *task)
{
  GPtrArray *aya_list;
  sqlite3_stmt *stmt;
  GqSura *sura;
  int id;

  g_assert (GQ_IS_QURAN_DB (self));
  g_assert (self->db);

  aya_list = g_ptr_array_new_with_free_func (g_object_unref);
  sura = g_object_get_data (G_OBJECT (task), "sura");
  g_assert (GQ_IS_SURA (sura));
  id = gq_sura_get_index (sura);

  sqlite3_prepare_v2 (self->db,
                      "SELECT aya_index,aya_index_ar,text FROM aya WHERE sura_id=? ORDER BY aya_index;",
                      -1, &stmt, NULL);
  sqlite3_bind_int (stmt, 1, id);
  while (sqlite3_step (stmt) == SQLITE_ROW)
    {
      const char *text, *index_ar;
      GqAya *aya;

      id = sqlite3_column_int (stmt, 0);
      index_ar = (gpointer)sqlite3_column_text (stmt, 1);
      text = (gpointer)sqlite3_column_text (stmt, 2);

      aya = gq_aya_new (sura, text, id, index_ar);
      g_ptr_array_add (aya_list, aya);
    }

  g_task_return_pointer (task, aya_list, (GDestroyNotify)g_ptr_array_unref);
}

static void
gq_quran_db_load (GqQuranDb *self,
                  GTask     *task)
{
  GqQuran *quran;
  sqlite3 *db;
  int status, version = 0;
  gboolean db_exists;

  g_assert (GQ_IS_QURAN_DB (self));
  g_assert (G_IS_TASK (task));
  g_assert (g_thread_self () == self->worker_thread);
  g_assert (self->db_path);
  g_assert (!self->db);

  db_exists = g_file_test (self->db_path, G_FILE_TEST_EXISTS);

  status = sqlite3_open (self->db_path, &db);
  g_assert (status == SQLITE_OK);

  if (db_exists)
    {
      version = gq_quran_db_get_db_version (self, db);

      if (version != DB_VERSION)
        {
          sqlite3_close (db);
          g_assert (g_remove (self->db_path) == 0);

          status = sqlite3_open (self->db_path, &db);
          g_assert (status == SQLITE_OK);
        }
    }

  if (version != DB_VERSION)
    {
      if (!gq_quran_db_create_schema (self, db, task) ||
          !gq_quran_populate_db (self, db, task))
        {
          sqlite3_close (db);
          return;
        }
    }

  self->db = db;
  quran = gq_quran_db_load_quran (self);
  g_task_return_pointer (task, quran, g_object_unref);
}

static gpointer
gq_quran_db_worker (gpointer user_data)
{
  GqQuranDb *self = user_data;

  g_assert (GQ_IS_QURAN_DB (self));

  do
    {
      g_autoptr(GTask) task = NULL;
      GqQuranDbCallback callback;

      task = g_async_queue_pop (self->queue);

      g_assert (task);
      callback = g_task_get_task_data (task);
      callback (self, task);

      if (callback == gq_quran_db_close_db)
        break;
  } while (true);

  return NULL;
}

static void
gq_quran_db_finalize (GObject *object)
{
  GqQuranDb *self = (GqQuranDb *)object;

  if (self->db)
    g_warning ("Database not closed");

  g_clear_pointer (&self->queue, g_async_queue_unref);
  g_free (self->db_path);

  G_OBJECT_CLASS (gq_quran_db_parent_class)->finalize (object);
}

static void
gq_quran_db_class_init (GqQuranDbClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = gq_quran_db_finalize;
}

static void
gq_quran_db_init (GqQuranDb *self)
{
  self->queue = g_async_queue_new ();
}

GqQuranDb *
gq_quran_db_new (char       *dir,
                 const char *file_name)
{
  GqQuranDb *self;

  g_return_val_if_fail (dir && *dir == '/', NULL);
  g_return_val_if_fail (file_name && *file_name, NULL);

  self = g_object_new (GQ_TYPE_QURAN_DB, NULL);

  g_mkdir_with_parents (dir, S_IRWXU);
  self->db_path = g_build_filename (dir, file_name, NULL);

  return self;
}

void
gq_quran_db_open_async (GqQuranDb           *self,
                        GCancellable        *cancellable,
                        GAsyncReadyCallback  callback,
                        gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;

  g_return_if_fail (GQ_IS_QURAN_DB (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, NULL, callback, user_data);
  g_task_set_source_tag (task, gq_quran_db_open_async);
  g_task_set_task_data (task, gq_quran_db_open_db, NULL);

  if (self->db) {
    g_warning ("A DataBase is already open");
    g_task_return_new_error (task, G_IO_ERROR, G_IO_ERROR_ALREADY_MOUNTED,
                             "Database is already open");
    return;
  }

  if (!self->worker_thread)
    self->worker_thread = g_thread_new ("quran-db-worker",
                                        gq_quran_db_worker,
                                        self);

  g_async_queue_push (self->queue, g_steal_pointer (&task));
}

gboolean
gq_quran_db_open_finish (GqQuranDb     *self,
                         GAsyncResult  *result,
                         GError       **error)
{
  g_return_val_if_fail (GQ_IS_QURAN_DB (self), FALSE);
  g_return_val_if_fail (G_IS_TASK (result), FALSE);
  g_return_val_if_fail (!error || !*error, FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

void
gq_quran_db_load_async (GqQuranDb           *self,
                        GCancellable        *cancellable,
                        GAsyncReadyCallback  callback,
                        gpointer             user_data)
{
  GTask *task;

  g_return_if_fail (GQ_IS_QURAN_DB (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));
  g_return_if_fail (self->db_path);

  task = g_task_new (self, NULL, callback, user_data);
  g_task_set_source_tag (task, gq_quran_db_load_async);
  g_task_set_task_data (task, gq_quran_db_load, NULL);
  g_async_queue_push (self->queue, task);
}

gpointer
gq_quran_db_load_finish (GqQuranDb     *self,
                         GAsyncResult  *result,
                         GError       **error)
{
  g_return_val_if_fail (GQ_IS_QURAN_DB (self), FALSE);
  g_return_val_if_fail (G_IS_TASK (result), FALSE);
  g_return_val_if_fail (!error || !*error, FALSE);

  return g_task_propagate_pointer (G_TASK (result), error);
}

void
gq_quran_db_load_sura_async (GqQuranDb           *self,
                             gpointer             sura,
                             GCancellable        *cancellable,
                             GAsyncReadyCallback  callback,
                             gpointer             user_data)
{
  GTask *task;

  g_return_if_fail (GQ_IS_QURAN_DB (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));
  g_return_if_fail (self->db_path);
  g_return_if_fail (GQ_IS_SURA (sura));

  task = g_task_new (self, NULL, callback, user_data);
  g_object_set_data_full (G_OBJECT (task), "sura",
                          g_object_ref (sura), g_object_unref);
  g_task_set_source_tag (task, gq_quran_db_load_sura_async);
  g_task_set_task_data (task, gq_quran_db_load_sura, NULL);
  g_async_queue_push (self->queue, task);
}

gpointer
gq_quran_db_load_sura_finish (GqQuranDb     *self,
                              GAsyncResult  *result,
                              GError       **error)
{
  g_return_val_if_fail (GQ_IS_QURAN_DB (self), FALSE);
  g_return_val_if_fail (G_IS_TASK (result), FALSE);
  g_return_val_if_fail (!error || !*error, FALSE);

  return g_task_propagate_pointer (G_TASK (result), error);
}
