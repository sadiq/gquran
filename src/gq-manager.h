/* gq-manager.h
 *
 * Copyright 2024 Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * Author(s):
 *   Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gio/gio.h>
#include <stdbool.h>

#include "quran/gq-quran.h"

G_BEGIN_DECLS

#define GQ_TYPE_MANAGER (gq_manager_get_type ())
G_DECLARE_FINAL_TYPE (GqManager, gq_manager, GQ, MANAGER, GObject)

GqManager   *gq_manager_new                 (void);
GqQuran     *gq_manager_get_quran           (GqManager            *self);
void         gq_manager_load_async          (GqManager            *self,
                                             bool                  force_reload,
                                             GCancellable         *cancellable,
                                             GAsyncReadyCallback   callback,
                                             gpointer              user_data);
gboolean     gq_manager_load_finish         (GqManager            *self,
                                             GAsyncResult         *result,
                                             GError              **error);

void         gq_manager_load_sura_async      (GqManager           *self,
                                              gpointer             sura,
                                              GCancellable        *cancellable,
                                              GAsyncReadyCallback  callback,
                                              gpointer             user_data);
gboolean     gq_manager_load_sura_finish     (GqManager           *self,
                                              GAsyncResult        *result,
                                              GError             **error);

G_END_DECLS
