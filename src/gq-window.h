/* gq-window.h
 *
 * Copyright 2024 Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * Author(s):
 *   Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <adwaita.h>

#include "gq-settings.h"
#include "gq-manager.h"

G_BEGIN_DECLS

#define GQ_TYPE_WINDOW (gq_window_get_type ())

G_DECLARE_FINAL_TYPE (GqWindow, gq_window, GQ, WINDOW, AdwApplicationWindow)

GtkWidget *gq_window_new (GtkApplication *application,
                          GqSettings     *settings,
                          GqManager      *manager);


G_END_DECLS
