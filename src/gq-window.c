/* gq-window.c
 *
 * Copyright 2024 Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * Author(s):
 *   Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "gq-window"

#ifdef HAVE_CONFIG_H
# include "config.h"
# include "version.h"
#endif

#include <glib/gi18n.h>

#include "gq-main-view.h"
#include "gq-sidebar.h"
#include "gq-window.h"
#include "gq-log.h"

struct _GqWindow
{
  AdwApplicationWindow  parent_instance;

  GqSettings           *settings;
  GqManager            *manager;
  GCancellable         *cancellable;

  GtkWidget            *window_title;
  GtkWidget            *menu_button;

  GtkWidget            *split_view;
  GtkWidget            *sidebar;
  GtkWidget            *main_view;
};

G_DEFINE_TYPE (GqWindow, gq_window, ADW_TYPE_APPLICATION_WINDOW)


enum {
  PROP_0,
  PROP_SETTINGS,
  N_PROPS
};

static GParamSpec *properties[N_PROPS];

static void
gq_window_show_about (GqWindow *self)
{
  const char *developers[] = {
    "Mohammed Sadiq https://www.sadiqpk.org",
    NULL
  };

  g_assert (GQ_IS_WINDOW (self));

  adw_show_about_window_from_appdata (GTK_WINDOW (self),
                                      "/org/sadiqpk/GQuran/metainfo",
                                      NULL,
                                      "version", PACKAGE_VERSION,
                                      "copyright", _("Copyright © 2024 Mohammed Sadiq"),
                                      "developers", developers,
                                      "translator-credits", _("translator-credits"),
                                      NULL);
}

static void
window_sidebar_collapsed_cb (GqWindow *self)
{
  g_assert (GQ_IS_WINDOW (self));

  if (gq_main_view_get_sura (GQ_MAIN_VIEW (self->main_view)))
    adw_overlay_split_view_set_show_sidebar (ADW_OVERLAY_SPLIT_VIEW (self->split_view), FALSE);
}

static void
window_sura_selected_cb (GObject      *object,
                         GAsyncResult *result,
                         gpointer      user_data)
{
  GqWindow *self = user_data;
  GqSura *sura;

  g_assert (GQ_IS_WINDOW (self));

  sura = gq_sidebar_get_selected_item (GQ_SIDEBAR (self->sidebar));
  gq_main_view_set_sura (GQ_MAIN_VIEW (self->main_view), sura);
  adw_window_title_set_title (ADW_WINDOW_TITLE (self->window_title),
                              gq_sura_get_name (sura));
}

static void
window_selection_changed_cb (GqWindow *self)
{
  GqSura *sura;

  g_assert (GQ_IS_WINDOW (self));

  sura = gq_sidebar_get_selected_item (GQ_SIDEBAR (self->sidebar));

  adw_overlay_split_view_set_show_sidebar (ADW_OVERLAY_SPLIT_VIEW (self->split_view), FALSE);
  gq_manager_load_sura_async (self->manager, sura, NULL, window_sura_selected_cb, self);
}

static void
gq_window_unmap (GtkWidget *widget)
{
  GqWindow *self = (GqWindow *)widget;
  GtkWindow *window = (GtkWindow *)widget;
  GdkRectangle geometry;
  gboolean is_maximized;

  is_maximized = gtk_window_is_maximized (window);
  gq_settings_set_window_maximized (self->settings, is_maximized);

  if (!is_maximized)
    {
      g_object_get (self, "default-width", &geometry.width, NULL);
      g_object_get (self, "default-height", &geometry.height, NULL);
      gq_settings_set_window_geometry (self->settings, &geometry);
    }

  GTK_WIDGET_CLASS (gq_window_parent_class)->unmap (widget);
}

static void
gq_window_set_property (GObject      *object,
                         guint         prop_id,
                         const GValue *value,
                         GParamSpec   *pspec)
{
  GqWindow *self = (GqWindow *)object;

  switch (prop_id)
    {
    case PROP_SETTINGS:
      self->settings = g_value_dup_object (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
gq_window_constructed (GObject *object)
{
  GqWindow *self = (GqWindow *)object;
  GtkWindow *window = (GtkWindow *)object;
  GdkRectangle geometry;

  gq_settings_get_window_geometry (self->settings, &geometry);
  gtk_window_set_default_size (window, geometry.width, geometry.height);

  if (gq_settings_get_window_maximized (self->settings))
    gtk_window_maximize (window);

  G_OBJECT_CLASS (gq_window_parent_class)->constructed (object);
}

static void
gq_window_finalize (GObject *object)
{
  GqWindow *self = (GqWindow *)object;

  GQ_TRACE_MSG ("finalizing window");

  g_cancellable_cancel (self->cancellable);
  g_clear_object (&self->cancellable);

  g_clear_object (&self->manager);
  g_clear_object (&self->settings);

  G_OBJECT_CLASS (gq_window_parent_class)->finalize (object);
}

static void
gq_window_class_init (GqWindowClass *klass)
{
  GObjectClass   *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->set_property = gq_window_set_property;
  object_class->constructed  = gq_window_constructed;
  object_class->finalize     = gq_window_finalize;

  widget_class->unmap = gq_window_unmap;

  /**
   * GqWindow:settings:
   *
   * The Application Settings
   */
  properties[PROP_SETTINGS] =
    g_param_spec_object ("settings",
                         "Settings",
                         "The Application Settings",
                         GQ_TYPE_SETTINGS,
                         G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_template_from_resource (widget_class,
                                               "/org/sadiqpk/GQuran/"
                                               "ui/gq-window.ui");

  gtk_widget_class_bind_template_child (widget_class, GqWindow, window_title);
  gtk_widget_class_bind_template_child (widget_class, GqWindow, menu_button);

  gtk_widget_class_bind_template_child (widget_class, GqWindow, split_view);
  gtk_widget_class_bind_template_child (widget_class, GqWindow, sidebar);
  gtk_widget_class_bind_template_child (widget_class, GqWindow, main_view);

  gtk_widget_class_bind_template_callback (widget_class, window_sidebar_collapsed_cb);
  gtk_widget_class_bind_template_callback (widget_class, window_selection_changed_cb);

  gtk_widget_class_install_action (widget_class, "win.about", NULL,
                                   (GtkWidgetActionActivateFunc)gq_window_show_about);

  g_type_ensure (GQ_TYPE_SIDEBAR);
  g_type_ensure (GQ_TYPE_MAIN_VIEW);
}

static void
gq_window_init (GqWindow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  self->cancellable = g_cancellable_new ();
}

static void
manager_load_cb (GObject      *object,
                 GAsyncResult *result,
                 gpointer      user_data)
{
  g_autoptr(GqWindow) self = user_data;
  g_autoptr(GError) error = NULL;
  GqQuran *quran;

  g_assert (GQ_IS_WINDOW (self));

  gq_manager_load_finish (self->manager, result, &error);

  quran = gq_manager_get_quran (self->manager);
  gq_sidebar_set_quran (GQ_SIDEBAR (self->sidebar), quran);
}

GtkWidget *
gq_window_new (GtkApplication *application,
               GqSettings     *settings,
               GqManager      *manager)
{
  GqWindow *self;

  g_assert (GTK_IS_APPLICATION (application));
  g_assert (GQ_IS_SETTINGS (settings));

  self = g_object_new (GQ_TYPE_WINDOW,
                       "application", application,
                       "settings", settings,
                       NULL);
  self->manager = g_object_ref (manager);

  gq_manager_load_async (manager, false, self->cancellable,
                         manager_load_cb, g_object_ref (self));

  return GTK_WIDGET (self);
}
