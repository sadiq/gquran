/* gq-manager.c
 *
 * Copyright 2024 Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * Author(s):
 *   Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "gq-manager"

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "quran/gq-quran-db.h"
#include "gq-manager.h"

struct _GqManager
{
  GObject    parent_instance;

  GqQuran   *quran;
  GqManager *manager;
  GqQuranDb *quran_db;

  gboolean  db_ready;
  bool      is_loading;
  bool      db_load_pending;
};


G_DEFINE_TYPE (GqManager, gq_manager, G_TYPE_OBJECT)

static void
gq_manager_finalize (GObject *object)
{
  GqManager *self = (GqManager *)object;

  g_clear_object (&self->quran);
  g_clear_object (&self->manager);
  g_clear_object (&self->quran_db);

  G_OBJECT_CLASS (gq_manager_parent_class)->finalize (object);
}

static void
gq_manager_class_init (GqManagerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = gq_manager_finalize;
}

static void
gq_manager_init (GqManager *self)
{
  char *db_path;

  db_path =  g_build_filename (g_get_user_data_dir (), PACKAGE_ID, "db", NULL);
  self->quran_db = gq_quran_db_new (db_path, "quran.db");
}

GqManager *
gq_manager_new (void)
{
  return g_object_new (GQ_TYPE_MANAGER, NULL);
}

GqQuran *
gq_manager_get_quran (GqManager *self)
{
  g_return_val_if_fail (GQ_IS_MANAGER (self), NULL);

  return self->quran;
}

static void
manager_load_db_cb (GObject      *object,
                    GAsyncResult *result,
                    gpointer      user_data)
{
  g_autoptr(GTask) task = user_data;
  GError *error = NULL;
  GqManager *self;

  g_assert (G_IS_TASK (task));

  self = g_task_get_source_object (task);
  g_assert (GQ_IS_MANAGER (self));

  self->is_loading = false;
  self->quran = gq_quran_db_load_finish (self->quran_db, result, &error);

  if (error)
    g_task_return_error (task, error);
  else
    g_task_return_boolean (task, TRUE);
}

static void
manager_open_db_cb (GObject      *object,
                    GAsyncResult *result,
                    gpointer      user_data)
{
  g_autoptr(GTask) task = user_data;
  GError *error = NULL;
  GqManager *self;

  g_assert (G_IS_TASK (task));

  self = g_task_get_source_object (task);
  g_assert (GQ_IS_MANAGER (self));

  self->db_ready = gq_quran_db_open_finish (self->quran_db, result, &error);

  if (error &&
      !g_error_matches (error, G_IO_ERROR, G_IO_ERROR_NOT_INITIALIZED))
    {
      g_task_return_error (task, error);
    }
  else
    {
      GCancellable *cancellable;

      cancellable = g_task_get_cancellable (task);

      gq_quran_db_load_async (self->quran_db, cancellable,
                              manager_load_db_cb, g_steal_pointer (&task));
    }
}

void
gq_manager_load_async (GqManager           *self,
                       bool                 force_reload,
                       GCancellable        *cancellable,
                       GAsyncReadyCallback  callback,
                       gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;

  g_return_if_fail (GQ_IS_MANAGER (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));
  g_return_if_fail (!self->is_loading);

  task = g_task_new (self, cancellable, callback, user_data);
  self->is_loading = true;

  if (self->quran)
    {
      self->is_loading = false;
      g_task_return_boolean (task, TRUE);
    }
  else if (!self->db_ready)
    {
      gq_quran_db_open_async (self->quran_db, cancellable,
                              manager_open_db_cb, g_steal_pointer (&task));
    }
  else
    {
      g_return_if_fail (force_reload);
      gq_quran_db_load_async (self->quran_db, cancellable,
                              manager_load_db_cb, g_steal_pointer (&task));
    }
}

gboolean
gq_manager_load_finish (GqManager     *self,
                        GAsyncResult  *result,
                        GError       **error)
{
  g_return_val_if_fail (GQ_IS_MANAGER (self), FALSE);
  g_return_val_if_fail (G_IS_TASK (result), FALSE);
  g_return_val_if_fail (!error || !*error, FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
load_sura_cb (GObject      *object,
              GAsyncResult *result,
              gpointer      user_data)
{
  g_autoptr(GTask) task = user_data;
  GError *error = NULL;
  GqManager *self;
  GPtrArray *ayas;
  GqSura *sura;

  g_assert (G_IS_TASK (task));

  self = g_task_get_source_object (task);
  g_assert (GQ_IS_MANAGER (self));

  sura = g_task_get_task_data (task);
  ayas = gq_quran_db_load_sura_finish (self->quran_db, result, &error);

  if (error)
    {
      g_task_return_error (task, error);
    }
  else
    {
      gq_sura_set_ayas (sura, ayas);
      g_task_return_boolean (task, TRUE);
    }
}

void
gq_manager_load_sura_async (GqManager           *self,
                            gpointer             sura,
                            GCancellable        *cancellable,
                            GAsyncReadyCallback  callback,
                            gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;

  g_return_if_fail (GQ_IS_MANAGER (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));
  g_return_if_fail (!self->is_loading);
  g_return_if_fail (GQ_IS_SURA (sura));
  g_assert (self->quran);
  g_assert (self->quran_db);

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_task_data (task, g_object_ref (sura), g_object_unref);

  if (gq_sura_get_ayas (sura))
    {
      g_task_return_boolean (task, TRUE);
      return;
    }

  gq_quran_db_load_sura_async (self->quran_db, sura, cancellable,
                               load_sura_cb,
                               g_steal_pointer (&task));
}

gboolean
gq_manager_load_sura_finish (GqManager     *self,
                             GAsyncResult  *result,
                             GError       **error)
{
  g_return_val_if_fail (GQ_IS_MANAGER (self), FALSE);
  g_return_val_if_fail (G_IS_TASK (result), FALSE);
  g_return_val_if_fail (!error || !*error, FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}
