/* main.c
 *
 * Copyright 2024 Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * Author(s):
 *   Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <gtk/gtk.h>
#include <libintl.h>
#include <locale.h>

#include "gq-utils.h"
#include "gq-application.h"
#include "gq-log.h"

int
main (int   argc,
      char *argv[])
{
  g_autoptr(GqApplication) application = NULL;

  g_assert (GQ_IS_MAIN_THREAD ());

  g_set_prgname (PACKAGE_NAME);
  gq_log_init ();

  application = gq_application_new ();
  setlocale (LC_ALL, "");
  bindtextdomain (GETTEXT_PACKAGE, PACKAGE_LOCALE_DIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
  textdomain (GETTEXT_PACKAGE);

  return g_application_run (G_APPLICATION (application), argc, argv);
}
