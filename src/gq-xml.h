/* bjb-xml.h
 *
 * Copyright 2024 Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * Author(s):
 *   Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>
#include <libxml/xmlreader.h>
#include <libxml/xmlwriter.h>

G_BEGIN_DECLS

typedef xmlTextReader XmlReader;
typedef xmlDoc XmlDoc;
typedef xmlNode XmlNode;
typedef xmlChar XmlChar;

G_DEFINE_AUTOPTR_CLEANUP_FUNC (XmlDoc, xmlFreeDoc)
G_DEFINE_AUTOPTR_CLEANUP_FUNC (XmlChar, xmlFree)

static inline xmlDoc *
xml_doc_new (const char *data,
             int        size)
{
  return xmlReadMemory (data, size , "", "utf-8",
                        XML_PARSE_RECOVER | XML_PARSE_NONET);
}

static inline xmlNode *
xml_doc_get_root (xmlDoc *doc)
{
  return xmlDocGetRootElement (doc);
}

static inline xmlChar *
xml_node_get_prop (xmlNode    *node,
                   const char *name)
{
  return xmlGetProp (node, (xmlChar *)name);
}

static inline int
xml_node_get_int (xmlNode    *node,
                  const char *name)
{
  g_autoptr(XmlChar) prop = NULL;

  prop = xml_node_get_prop (node, name);

  return g_ascii_strtod ((char *)prop, NULL);
}


G_END_DECLS
