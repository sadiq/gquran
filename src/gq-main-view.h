/* gq-main-view.c
 *
 * Copyright 2024 Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * Author(s):
 *   Mohammed Sadiq <sadiq@sadiqpk.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>

#include "quran/gq-sura.h"

G_BEGIN_DECLS

#define GQ_TYPE_MAIN_VIEW (gq_main_view_get_type ())
G_DECLARE_FINAL_TYPE (GqMainView, gq_main_view, GQ, MAIN_VIEW, GtkBox)

GqSura *gq_main_view_get_sura (GqMainView *self);
void    gq_main_view_set_sura (GqMainView *self,
                               GqSura     *sura);

G_END_DECLS
